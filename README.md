This file provides a c++ driver class to drive adressable LEDs. The driver
currently only supports WS2812 leds. 
Asides driving the hardware, this library further provides a structure to 
schedule the display of patterns. A Pattern is a sequence of led blinkings. 
The pattern must be provided as a function. The function must have a pointer
as argument (can be $NULL if the function does not use it). This pointer is 
usually used to point to a specific argument or an argument list and cast 
accordingly. To ensure consistency across runs this argument pointer is 
recorded in the pattern structures. 
If a function dynamically allocates memory that remains allocated across 
executions of the pattern, a clenaup function must be provided as well in 
order to prevent memory leaks. Note that once a pattern is successfully put
forward for displaying, all of the datastructures are now owned by the LED
pattern display thread. Under no circumstance you shall reuse any 
dynamically allocated memory references. The pattern structure itself can be 
safely delted or reused, as it was copied when sent to the LED display 
thread. For convenience, a pattern generator is provided. This allows the 
generation of patterns consisting of a list of states with execution time. 
This allows dynamic pattern generation. I the pattern is known at time of 
programming then it should be preffered to directly provide a function 
rather than using the generator, as this usually is more efficient.
